'use strict';

angular.module('velociusApp')
  .controller('LoginCtrl', function ($scope, Auth, $window) {
    // Remove the hash that gets appended after canceled login
    if ($window.location.href.indexOf('#') !== -1) {
      $window.history.replaceState("",
        document.title, window.location.pathname);
    }

    $scope.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider;
    };
  });

// After login, redirect to home page.
angular.module('velociusApp')
  .controller('PostLoginCtrl', function ($scope, $location) {
    $location.path('/');
  });
