'use strict';

angular.module('velociusApp')
  .controller('SettingsCtrl', function ($scope, $location, $q, User, Section, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
      $scope.isNotAdmin = !$scope.isAdmin;
      $scope.user = Auth.getCurrentUser();
    });

    $scope.sections = Section.query(function (sections) {
      $scope.sectionsDict = Section.makeDict(sections);
    });

    $scope.setSection = function(sectionId) {
      if ($scope.isNotAdmin || sectionId === $scope.user.section._id) {
        return;
      }
      var d = $q.defer();
      User.update(
        {id: $scope.user._id, controller: "section"},
        {section: sectionId},
        function success() {
          $scope.user.section = $scope.sectionsDict[sectionId];
          d.resolve();
        },
        function error(res) {
          d.reject("An error occurred while setting the user section.");
        }
      );
      return d.promise;
    };

    $scope.logout = function() {
      Auth.logout();
      $location.path('/');
    };

    /*
    $scope.errors = {};

    $scope.changePassword = function(form) {
      $scope.submitted = true;
      if(form.$valid) {
        Auth.changePassword( $scope.user.oldPassword, $scope.user.newPassword )
        .then( function() {
          $scope.message = 'Password successfully changed.';
        })
        .catch( function() {
          form.password.$setValidity('mongoose', false);
          $scope.errors.other = 'Incorrect password';
          $scope.message = '';
        });
      }
    };
    */
  });
