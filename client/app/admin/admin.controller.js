'use strict';

angular.module('velociusApp')
  .controller('AdminCtrl', function ($scope, User, Text, Gloss, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
    });

    $scope.refreshTexts = function() {
      Text.refresh();
    };

    $scope.refreshGlosses = function() {
      Gloss.refresh();
    };

    $scope.refreshUsers = function() {
      User.refresh();
    };
  });

angular.module('velociusApp')
  .controller('AdminUsersCtrl', function ($scope, $q, User, Section, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
    });
    $scope.userRoles = userRoles;
    $scope.users = User.query();

    $scope.sections = Section.query(function (sections) {
      $scope.sectionsDict = Section.makeDict(sections);
    });

    $scope.setUserRole = function(user, role) {
      if ($scope.userRoles.indexOf(role) === -1) {
        return "Please select a valid access role";
      } else if (role === user.role) {
        return;
      }
      var d = $q.defer();
      User.update(
        {id: user._id, controller: "role"},
        {role: role},
        function success() {
          d.resolve();
        },
        function error(res) {
          d.reject("An error occurred while setting the user role.");
        }
      );
      return d.promise;
    };

    $scope.setUserSection = function(user, sectionId) {
      if (sectionId === user.section._id) {
        return;
      }
      var d = $q.defer();
      User.update(
        {id: user._id, controller: "section"},
        {section: sectionId},
        function success() {
          user.section = $scope.sectionsDict[sectionId];
          d.resolve();
        },
        function error(res) {
          d.reject("An error occurred while setting the user section.");
        }
      );
      return d.promise;
    };

    $scope.deleteUser = function(user) {
      User.remove({ id: user._id });
      angular.forEach($scope.users, function(u, i) {
        if (u === user) {
          $scope.users.splice(i, 1);
        }
      });
    };
  });

angular.module('velociusApp')
  .controller('AdminSectionsCtrl', function ($scope, $q, User, Section, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
    });

    $scope.loadSections = function() {
      $scope.sections = Section.query(function (sections) {
        $scope.sectionsDict = Section.makeDict(sections);
      });
    };

    $scope.section = new Section(); // section to add
    $scope.loadSections();

    $scope.addSection = function(form) {
      if (! $scope.section.name) {
        return;
      }
      $scope.section.$save(
        function success(data) {
          clearFormErrors(form);
          $scope.section = new Section();
          $scope.loadSections();
        },
        function error(res) {
          clearFormErrors(form);
          setFormErrors(form, res.data.errors);
        }
      );
    };

    $scope.renameSection = function(section, name) {
      console.log("renameSection: begin with name=" + name);
      if (name === "") {
        return "Please enter a section name";
      } else if (name === section.name) {
        console.log("renameSection: unchanged");
        return;
      }
      var d = $q.defer();
      Section.update(
        {sectionId: section._id},
        {name: name},
        function success() {
          console.log("renameSection: updated");
          d.resolve();
        },
        function error(res) {
          console.log("renameSection: update error");
          d.reject(res.data.errors['name'].message);
        }
      );
      return d.promise;
    };

    $scope.deleteSection = function(section) {
      // TODO
    };
  });
