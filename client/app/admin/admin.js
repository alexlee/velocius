'use strict';

angular.module('velociusApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin', {
        templateUrl: 'app/admin/admin.html',
        controller: 'AdminCtrl'
      })
      .when('/admin/users', {
        templateUrl: 'app/admin/users.html',
        controller: 'AdminUsersCtrl'
      })
      .when('/admin/sections', {
        templateUrl: 'app/admin/sections.html',
        controller: 'AdminSectionsCtrl'
      });
  });
