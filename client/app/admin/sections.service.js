'use strict';

// TODO
// Use $cacheFactory?

angular.module('velociusApp')
  .factory('Section', function ($resource) {
    var Section = $resource(
      '/api/sections/:sectionId',
      {
        sectionId: '@_id'
      },
      {
        update: { method: 'PUT' },
        getDefault: {
          method: 'GET',
          url: '/api/sections/default'
        }
      });
    Section.makeDict = function(sections) {
      var dict = {};
      var sec;
      for (var i = 0; i < sections.length; i++) {
        sec = sections[i];
        dict[sec._id] = sec;
      }
      return dict;
    };
    return Section;
  });
