'use strict';

angular.module('velociusApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'btford.socket-io',
  'ng-showdown',
  'xeditable',
  'ngAnimate',
  'mgcrea.ngStrap'
])
  .config(function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');
  })

  .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if(response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        }
        else {
          return $q.reject(response);
        }
      }
    };
  })

  .run(function ($rootScope, $location, Auth) {
    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$routeChangeStart', function (event, next) {
      Auth.isLoggedInAsync(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  })

  .run(function(editableOptions) {
    editableOptions.theme = 'bs3';
  })

  .run(function($rootScope, Section) {
    $rootScope.default_section = Section.getDefault();
  });

/**
 * Additional processing for `syncUpdates`.
 *
 * We want each new item to be a proper Resource object, and not just
 * a generic Object.
 *
 * See `client/components/socket/socket.service.js`.
 */
function afterSyncUpdate(ResourceType) {
  return function(event, item, array) {
    if (event === 'created') {
      var index = array.indexOf(item);
      var resource = new ResourceType(item);
      array.splice(index, 1, resource);
    }
  };
}
