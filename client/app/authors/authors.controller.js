'use strict';

angular.module('velociusApp')
  .controller('AuthorsCtrl', function ($scope, Author, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
    });
    $scope.author = new Author();
    $scope.authors = Author.query();

    $scope.addAuthor = function(form) {
      if (! $scope.author.name) {
        return;
      }
      $scope.author.$save(
        function success(data) {
          clearFormErrors(form);
          $scope.author = new Author();
          $scope.authors = Author.query();
        },
        function error(res) {
          clearFormErrors(form);
          setFormErrors(form, res.data.errors);
        }
      );
    };

    $scope.dismissAlert = function(formitem) {
      formitem.$error.mongoose = null;
    };
  });

function setAccessFlags(role, texts) {
}

angular.module('velociusApp')
  .controller('AuthorViewCtrl', function ($scope, $routeParams, $location,
                                          $q, Author, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
    });
    $scope.role = Auth.getCurrentUser().role || 'guest';
    $scope.author = Author.get({ authorId: $routeParams.authorId });
    $scope.deleteError = null;

    $scope.authoredTexts = Author.query({
      authorId: $routeParams.authorId,
      action: 'authored'
    }, textAccessibilityFn($scope.role));

    $scope.editedTexts = Author.query({
      authorId: $routeParams.authorId,
      action: 'edited'
    }, textAccessibilityFn($scope.role));

    $scope.renameAuthor = function(name) {
      if (name === "") {
        return "Please enter an author name";
      } else if (name === $scope.author.name) {
        return;
      }
      var d = $q.defer();
      Author.update(
        {authorId: $scope.author._id},
        {name: name},
        function success() { 
          d.resolve();
        },
        function error(res) {
          d.reject(res.data.errors['name'].message);
        }
      );
      return d.promise;
    };

    $scope.deleteAuthor = function(author) {
      $scope.deleteError = null;
      author.$delete(
        function success() {
          $location.path('/authors');
          $location.replace();
        },
        function error(res) {
          $scope.deleteError = res.data;
        }
      );
    };
  });

angular.module('velociusApp')
  .directive('velAuthorList', function() {
    return {
      restrict: 'E',
      scope: {
        authors: '=list'
      },
      templateUrl: 'app/authors/authorList.html'
    };
  });
