'use strict';

angular.module('velociusApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/authors', {
        templateUrl: 'app/authors/authors.html',
        controller: 'AuthorsCtrl'
      })
      .when('/authors/:authorId', {
        templateUrl: 'app/authors/authorView.html',
        controller: 'AuthorViewCtrl'
      });
  });
