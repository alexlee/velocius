'use strict';

angular.module('velociusApp')
  .factory('Author', function ($resource) {
    return $resource(
      '/api/authors/:authorId/:action',
      {
        authorId: '@_id',
        action: ''
      },
      {
        update: { method: 'PUT' }
      });
  });
