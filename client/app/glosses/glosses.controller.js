'use strict';

angular.module('velociusApp')
  .controller('GlossesCtrl', function ($scope, $q, Lang, Section, Gloss, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
      $scope.isNotAdmin = !$scope.isAdmin;
      if (loggedIn) $scope.section = Auth.getCurrentUser().section._id;
      else $scope.section = $scope.default_section._id;
    });

    $scope.langs = Lang.query();
    $scope.lang = 'la';
    $scope.sections = Section.query();
    $scope.glosses = [];

    $scope.$watch('lang', function(newLang) {
      console.log('lang switch: ' + newLang);
      $scope.glosses = Gloss.query({lang: newLang});
    });

    $scope.refreshGlosses = function() {
      $scope.glosses = Gloss.query({lang: $scope.lang});
    };

    /**
     * No empty fields are allowed.
     */
    $scope.checkField = function(data, id) {
      if (data === undefined || data === '') {
        console.log('checkField: error');
        return "Cannot be empty";
      } else {
        console.log('checkField: success');
        return true;
      }
    };

    /**
     * If cancel is pressed on a new, unsaved gloss, then remove it.
     */
    $scope.cancel = function(data, index, id) {
      if (id === undefined) {
        $scope.glosses.splice(index, 1);
      }
    };

    /**
     * Save new Gloss or else update existing Gloss.
     */
    $scope.saveGloss = function(data, index, id) {
      console.log('saveGloss: BEGIN, index='+index+', id='+id);
      if (id === undefined) {
        // Save new gloss
        var d = $q.defer();
        console.log('saveGloss: save new '+JSON.stringify(data));
        data.lang = $scope.lang;
        Gloss.save(
          {lang: $scope.lang},
          data,
          function success(gloss) {
            console.log('saveGloss: save new successful');
            $scope.glosses[index] = gloss;
            d.resolve();
          },
          function error(res) {
            console.log('saveGloss: save new error');
            console.log(JSON.stringify(res.data.errors));
            d.reject("Server error while saving new gloss");
          }
        );
        return d.promise;
      } else {
        return $scope.updateGloss(data, index, id);
      }
    };

    /**
     * Update existing gloss
     */
    $scope.updateGloss = function(data, index, id) {
      // Update existing gloss
      console.log('saveGloss: update '+id);
      var d = $q.defer();
      Gloss.update(
        {lang: $scope.lang, glossId: id},
        data,
        function success(gloss) {
          console.log('saveGloss: update successful');
          $scope.glosses[index] = gloss;
          d.resolve();
        },
        function error(res) {
          console.log('saveGloss: update error');
          d.reject("Server error while updating gloss");
        }
      );
      return d.promise;
    };

    $scope.removeGloss = function(gloss) {
      var index = $scope.glosses.indexOf(gloss);
      $scope.glosses.splice(index, 1);
      gloss.$delete();
    };

    $scope.addGloss = function() {
      $scope.inserted = new Gloss({lang: $scope.lang});
      $scope.inserted.levels = [];
      $scope.glosses.push($scope.inserted);
    };
  });

// Returns a function to check whether gloss is at specified level
function glossFilterFactory(level) {
  return function hasLevel(gloss) {
    return (gloss.level === level);
  };
}

// Returns a function to check whether gloss is at specified level
// or shares the lemma of a gloss at the specified level
function glossSmartFilterFactory(level, lemmas) {
  return function hasLevel(gloss) {
    if (gloss.level === level) return true;
    else if (lemmas[gloss.lemma]) return true;
    else return false;
  };
}

// Creates a lookup table to see whether the given lemma has
// an associated gloss at the specified level
function lemmasAtLevel(level, glosses) {
  var lemmas = {};
  var g;
  var i = glosses.length;
  while (i--) {
    g = glosses[i];
    if (g.lemma && g.level === level) {
      lemmas[g.lemma] = true;
    }
  }
  return lemmas;
}

angular.module('velociusApp')
  .controller('GlossLevelsCtrl', function ($scope, $q, Lang, Section, Gloss, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
      if (loggedIn) $scope.section = Auth.getCurrentUser().section._id;
      else $scope.section = $scope.default_section._id;
    });

    $scope.langs = Lang.query();
    $scope.lang = 'la';
    $scope.sections = Section.query()

    $scope.glossesAll = []; // list of all glosses
    $scope.glosses = []; // currently shown subset of glosses

    $scope.$watch('lang', function(newLang) {
      console.log('lang switch: ' + newLang);
      $scope.glossesAll = Gloss.query({lang: newLang}, function success() {
        $scope.resetLevels();
        $scope.glosses = $scope.glossesAll;
      });
    });

    $scope.$watch('section', function(newSection) {
      console.log('section switch: ' + newSection);
      $scope.resetLevels();
    });

    // Simple gloss level filter
    $scope.loadGlosses = function(level) {
      if (level < 0 || level > 3) {
        $scope.glosses = $scope.glossesAll;
      } else {
        $scope.glosses = $scope.glossesAll.filter(
          glossFilterFactory(level)
        );
      }
    };

    // Smart, lemma-aware gloss level filter
    $scope.smartLoadGlosses = function(level) {
      if (level < 0 || level > 3) {
        $scope.glosses = $scope.glossesAll;
      } else {
        var lemmas = lemmasAtLevel(level, $scope.glossesAll);
        $scope.glosses = $scope.glossesAll.filter(
          glossSmartFilterFactory(level, lemmas)
        );
      }
    };

    $scope.resetLevels = function() {
      var glosses = $scope.glossesAll;
      var i = glosses.length;
      while (i--) {
        var gloss = glosses[i];
        gloss.level = gloss.getLevel($scope.section);
      }
    };

    $scope.updateLevel = function(gloss) {
      gloss.level = parseInt(gloss.level);
      Gloss.setLevel(
        {lang: gloss.lang, glossId: gloss._id},
        {section: $scope.section, level: gloss.level},
        function success(updatedGloss) {
          console.log(JSON.stringify(updatedGloss));
          updatedGloss.level = gloss.level;
          var i = $scope.glosses.indexOf(gloss);
          $scope.glosses[i] = updatedGloss;
        },
        function error(res) {
          console.log(JSON.stringify(res));
        }
      );
    };
  });
