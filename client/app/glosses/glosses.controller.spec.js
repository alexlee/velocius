'use strict';

describe('Controller: GlossesCtrl', function () {

  // load the controller's module
  beforeEach(module('velociusApp'));

  var GlossesCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GlossesCtrl = $controller('GlossesCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
