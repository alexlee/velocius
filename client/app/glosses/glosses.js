'use strict';

angular.module('velociusApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/glosses', {
        templateUrl: 'app/glosses/glosses.html',
        controller: 'GlossesCtrl'
      })
      .when('/glosses/levels', {
        templateUrl: 'app/glosses/glossLevels.html',
        controller: 'GlossLevelsCtrl'
      });
  });
