'use strict';

angular.module('velociusApp')
  .factory('Gloss', function ($resource) {
    var Gloss = $resource(
      '/api/glosses/:lang/:glossId',
      {
        lang: '@lang',
        glossId: '@_id'
      },
      {
        update: {
          method: 'PUT'
        },
        setLevel: {
          method: 'PUT',
          url: '/api/glosses/:lang/:glossId/level'
        },
        refresh: {
          method: 'GET',
          url: '/api/glosses/refresh'
        }
      });
    Gloss.prototype.getLevel = function(section) {
      if (! section) return 0;
      // lazy create a lookup dictionary
      if (! this.leveldict) {
        var dict = {};
        var setting;
        for (var i = 0; i < this.levels.length; i++) {
          setting = this.levels[i];
          dict[setting.section] = setting.level;
        }
        this.leveldict = dict;
      }
      // now find it
      var level = this.leveldict[section];
      return level ? level : 0;
    };
    Gloss.prototype.setCurrentLevel = function(section) {
      this.level = this.getLevel(section);
    };
    return Gloss;
  });
