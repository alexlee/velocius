'use strict';

describe('Service: glosses', function () {

  // load the service's module
  beforeEach(module('velociusApp'));

  // instantiate service
  var glosses;
  beforeEach(inject(function (_glosses_) {
    glosses = _glosses_;
  }));

  it('should do something', function () {
    expect(!!glosses).toBe(true);
  });

});
