/**
 * Word wrapper.
 *
 * Adapted from:
 * http://benalman.com/projects/jquery-replacetext-plugin/
 *
 * Operates recursively on its target objects.
 * Skips text that is already enclosed in a 'w'.
 *
 * Licensed under the MIT license.
 */

(function($){

  // Match individual words
  var UNICODE_WORD = XRegExp('(\\p{L}+)', 'g');

  // Wrap in a 'w' element
  var WRAP_WORD = function(m, p1, off, str) {
    return '<w>' + p1 + '<\/w>';
  };

  // Match any whitespace
  var WHITESPACE = XRegExp('(\\s+)', 'g');

  // Wrap in a 'span' element
  var WRAP_SPAN = function(m, p1, off, str) {
    return '<span>' + p1 + '<\/span>';
  };

  // Match punctuation that separates clauses
  var CLAUSE_SEPARATOR = XRegExp('([!?.:;])', 'g');

  // Wrap in a 'pc' element
  var WRAP_PC = function(m, p1, off, str) {
    return '<pc>' + p1 + '<\/pc>';
  };

  /**
  * Encloses words within text nodes in 'w' elements.
  * Returns the initial jQuery collection of elements.
  *
  * markPunctuation:
  *   Encloses some punctuation characters within text nodes in 'pc' elements.
  *   Returns the initial jQuery collection of elements.
  *
  *   This is used to mark sentence breaks, in order that glossary phrases
  *   can be correctly found within the text. It is not entirely precise,
  *   but it should be good enough for our purposes.
  *
  * protectSpaces:
  *   Encloses whitespace within text nodes in 'span' elements.
  *   Returns the initial jQuery collection of elements.
  *
  *   This is necessary to work around browser bugs, where DOM
  *   manipulations can cause whitespace to get lost between
  *   inline elements. We protect the whitespace by wrapping it
  *   in 'span' elements.
  */
  $.fn.wrapWords = function(options) {
    var defaults = {
      markPunctuation: false,
      protectSpaces: false
    };
    var options = $.extend({}, defaults, options);

    return this.each(function() {
      var node = this.firstChild;
      var val, new_val;

      // Elements to be removed at the end.
      var remove = [];
        
      // Only continue if firstChild exists.
      if (node) {

        // Loop over all childNodes.
        do {

          // Process text nodes.
          if (node.nodeType === 3) {
            val = node.nodeValue;
            new_val = val.replace(UNICODE_WORD, WRAP_WORD);
            
            if (options.markPunctuation) {
              new_val = new_val.replace(CLAUSE_SEPARATOR, WRAP_PC);
            }
            if (options.protectSpaces) {
              new_val = new_val.replace(WHITESPACE, WRAP_SPAN);
            }

            // Set the new value (which contains HTML).
            $(node).before(new_val);
            
            // Don't remove the node yet, or the loop will lose its place.
            remove.push(node);
          }
          // Recursively process element nodes.
          // Skip it if it's already a 'w' element.
          else if (node.nodeType === 1
                   && node.tagName.toLowerCase() !== 'w') {
            $(node).wrapWords(options);
          }
          
        } while (node = node.nextSibling);
      }
      
      // Time to remove those elements!
      remove.length && $(remove).remove();
    });
  };

})(jQuery);
