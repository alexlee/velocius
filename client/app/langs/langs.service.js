'use strict';

angular.module('velociusApp')
  .service('Lang', function ($resource) {
    return $resource('/api/langs');
  });
