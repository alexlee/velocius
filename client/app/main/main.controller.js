'use strict';

angular.module('velociusApp')
  .controller('MainCtrl', function ($scope, $window, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isLoggedIn = Auth.isLoggedIn();
      $scope.user = Auth.getCurrentUser();
    });

    $scope.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider;
    };
  });
