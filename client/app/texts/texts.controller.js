'use strict';

angular.module('velociusApp')
  .controller('TextsCtrl', function ($scope, Text, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
      $scope.role = Auth.getCurrentUser().role || 'guest';
      $scope.texts = Text.query(textAccessibilityFn($scope.role));
    });
  });

// TODO Create a service to handle shared code between controllers.

/**
 * Make the array of gloss data into an array of Gloss objects.
 */
function convertGlosses(glosses, Gloss) {
  var i = glosses.length;
  while (i--) {
    glosses[i] = new Gloss(glosses[i]);
  }
}

/**
 * Set the `level` attribute of each Gloss, based on the specified section.
 */
function setGlossLevels(glosses, section) {
  var i = glosses.length;
  while (i--) {
    glosses[i].setCurrentLevel(section);
  }
}

angular.module('velociusApp')
  .controller('TextViewCtrl', function ($scope, $routeParams, Text, Gloss, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
      $scope.user = Auth.getCurrentUser();
      var section = ($scope.user.section) ? $scope.user.section._id : undefined;

      Text.get({
        textId: $routeParams.textId, action: 'full'
      }, function success(data) {
        $scope.text = data.text;
        convertGlosses(data.glosses, Gloss);
        setGlossLevels(data.glosses, section);
        $scope.glosses = data.glosses;
      });
    });
  });

angular.module('velociusApp')
  .controller('TextEditCtrl', function ($scope, $routeParams, $q, Lang, Text, Gloss, Auth) {
    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isAdmin = Auth.isAdmin();
      $scope.user = Auth.getCurrentUser();
      var section = ($scope.user.section) ? $scope.user.section._id : undefined;

      Text.get({
        textId: $routeParams.textId, action: 'full'
      }, function success(data) {
        $scope.text = data.text;
        convertGlosses(data.glosses, Gloss);
        setGlossLevels(data.glosses, section);
        $scope.glosses = data.glosses;
      });
    });

    Lang.query(function(langs) {
      $scope.langs = langs;
      $scope.langdict = {};
      for (var i = 0; i < langs.length; i++) {
        var lang = langs[i];
        $scope.langdict[lang.short] = lang.long;
      }
    });
    $scope.userRoles = userRoles;

    $scope.renameText = function(title) {
      if (title === "") {
        return "Please enter a text title";
      } else if (title === $scope.text.title) {
        return;
      }
      var d = $q.defer();
      Text.update(
        {textId: $scope.text._id},
        {title: title},
        function success() {
          d.resolve();
        },
        function error(res) {
          d.reject("An error occurred while updating the text title.");
        }
      );
      return d.promise;
    };

    $scope.setTextLang = function(lang) {
      if (!$scope.langdict[lang]) {
        return "Please select a valid language";
      } else if (lang === $scope.text.lang) {
        return;
      }
      var d = $q.defer();
      Text.update(
        {textId: $scope.text._id},
        {lang: lang},
        function success() {
          d.resolve();
        },
        function error(res) {
          d.reject("An error occurred while setting the text language.");
        }
      );
      return d.promise;
    };

    $scope.setTextRole = function(role) {
      if ($scope.userRoles.indexOf(role) === -1) {
        return "Please select a valid access role";
      } else if (role === $scope.text.requiredRole) {
        return;
      }
      var d = $q.defer();
      Text.update(
        {textId: $scope.text._id},
        {requiredRole: role},
        function success() {
          d.resolve();
        },
        function error(res) {
          d.reject("An error occurred while setting the text access role.");
        }
      );
      return d.promise;
    };

    $scope.updateText = function(body) {
      if (body === $scope.text.body) {
        return;
      }
      var d = $q.defer();
      Text.update(
        {textId: $scope.text._id},
        {body: body},
        function success() {
          d.resolve();
        },
        function error(res) {
          d.reject("An error occurred while updating the text content.");
        }
      );
      return d.promise;
    };
  });

angular.module('velociusApp')
  .controller('TextCreateCtrl', function ($scope, $location, Lang, Text) {
    Lang.query(function(langs) {
      $scope.langs = langs;
    });
    $scope.userRoles = userRoles;
    $scope.text = new Text({ lang: 'la', requiredRole: 'guest' });

    $scope.createText = function(form) {
      console.log('createText: ' + JSON.stringify($scope.text));
      $scope.text.$save(
        function success(data) {
          clearFormErrors(form);
          $location.path('/texts');
          $location.replace();
        },
        function error(res) {
          clearFormErrors(form);
          setFormErrors(form, res.data.errors);
        });
    };
  });
