'use strict';

describe('Controller: TextsCtrl', function () {

  // load the controller's module
  beforeEach(module('velociusApp'));

  var TextsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TextsCtrl = $controller('TextsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
