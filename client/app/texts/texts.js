'use strict';

angular.module('velociusApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/texts', {
        templateUrl: 'app/texts/texts.html',
        controller: 'TextsCtrl'
      })
      .when('/texts/create', {
        templateUrl: 'app/texts/textCreate.html',
        controller: 'TextCreateCtrl'
      })
      .when('/texts/:textId', {
        templateUrl: 'app/texts/textView.html',
        controller: 'TextViewCtrl'
      })
      .when('/texts/:textId/edit', {
        templateUrl: 'app/texts/textEdit.html',
        controller: 'TextEditCtrl'
      });
  });

angular.module('velociusApp')
  .directive('velTextList', function() {
    return {
      restrict: 'E',
      scope: {
        texts: '=list',
        showAuthors: '='
      },
      templateUrl: 'app/texts/textList.html'
    };
  });

angular.module('velociusApp')
  .directive('velTextRender', function($compile, $showdown, Text) {
    return {
      restrict: 'E',
      scope: {
        text: '=',
        glosses: '=',
        section: '='
      },
      template: '<div class="textview"></div>',
      link: function(scope, element, attrs) {
        /*
         * For cleanup issues involved in compilation, see the following:
         * http://www.bennadel.com/blog/2706-always-trigger-the-destroy-event-before-removing-elements-in-angularjs-directives.htm
         * http://www.mattzeunert.com/2014/11/03/manually-removing-angular-directives.html
         */
        scope.glossary = {}; // dictionary
        var cloneScope = null;

        scope.$watch('text.body', function(newText) {
          if (newText) {
            console.log("velTextRender: link: set new text");
            var target = element.find('div.textview');
            var html = $showdown.makeHtml(newText);
            var newContent = angular.element(html);

            newContent.wrapWords({
              markPunctuation: true,
              protectSpaces: true // FIXME keep for now, but disable later
            });

            // Handle glosses, then attach popovers
            scope.glossary = createGlossary(scope.glosses);
            scope.glossLinks = createGlossLinks(newContent, scope.glossary);
            applyPopovers(newContent, scope.glossLinks);

            // If necessary, clean up previous scope
            if (cloneScope) {
              cloneScope.$destroy();
              cloneScope = null;
            }
            // Now replace with a fresh scope
            cloneScope = scope.$new();

            target.empty()
            target.append($compile(newContent)(cloneScope));
          }
        });
      }
    };
  });

angular.module('velociusApp')
  .config(function($popoverProvider) {
    angular.extend($popoverProvider.defaults, {
      placement: 'bottom',
      autoClose: 'true'
    });
  });

/**
 * Returns the highest level in the list of glosses.
 */
function lowestLevel (glosses) {
  var level = 3;
  var i = glosses.length;
  while (i--) {
    if (glosses[i].level < level) level = glosses[i].level;
  }
  return level;
}

/**
 * Applies a popover to all 'w' elements.
 */
function applyPopovers(target, glossLinks) {
  target.find('w, pc').reverse().each(function(index, element) {
    if (element.tagName === 'PC') { return; }
    var w = angular.element(element);
    if (glossLinks[index]) {
      var level = lowestLevel(glossLinks[index]);
      w.attr("bs-popover", "")
        .attr("data-content", index) // unique index
        .attr("data-template-url", "app/texts/popover.html")
        .addClass("gloss-"+level);
    } else {
      w.addClass("no-glosses");
    }
  });
}

function safePush(obj, key, item) {
  var list = obj[key];
  if (!list) {
    obj[key] = [item];
  } else {
    list.push(item);
  }
}

/**
 * Creates a glossary dictionary from the given array of glosses.
 */
function createGlossary(glosses) {
  var glossary = {};
  glosses.forEach(function(gloss, index) {
    var key = gloss.w0;
    var list = glossary[key];
    if (!list) {
      glossary[key] = [gloss];
    } else {
      list.push(gloss);
    }
  });
  return glossary;
}

// Checks if `left` exists within `right`
function phraseMatch(left, right) {
  function match(n) {
    if (n === left.length) return true;
    else if (left[n] === right[n]) return match(n + 1);
    else return false;
  }
  return match(0);
}

/**
 * Figures out which glosses go with each word.
 */
function createGlossLinks(target, glossary) {
  var glossLinks = {};
  var recent = []; // list of following words

  // When a phrase match is found, associates the gloss
  // with all words in that phrase
  function fillPhraseMatches(gloss, index) {
    var matches;
    for (var i = 1; i < gloss.length; i++) {
      safePush(glossLinks, index - i, gloss);
    }
  }

  // Returns all glosses that match the text at the given index
  function findMatches(glosses, index) {
    var matches = [];
    var i, gloss;
    for (i = 0; i < glosses.length; i++) {
      gloss = glosses[i];
      if (gloss.length === 1) {
        matches.push(gloss);
      } else {
        if (phraseMatch(gloss.words, recent)) {
          matches.push(gloss);
          fillPhraseMatches(gloss, index);
        }
      }
    }
    return matches;
  }

  // Process all words, going in reverse.
  // Use `pc` elements to detect sentence divisions.
  target.find('w, pc').reverse().each(function(index, element) {
    if (element.tagName === 'PC') {
      recent = [];
      return;
    }
    var word = angular.element(element).text().toLowerCase();
    // TODO process diacritics for Greek

    recent.unshift(word); // add this word to recent words list

    var glosses = glossary[word];
    if (!glosses) return;

    var matches = findMatches(glosses, index);
    if (matches.length) glossLinks[index] = matches;
  });
  return glossLinks;
}
