'use strict';

angular.module('velociusApp')
  .factory('Text', function ($resource) {
    return $resource(
      '/api/texts/:textId/:action',
      {
        textId: '@_id',
      },
      {
        update: { method: 'PUT' },
        refresh: {
          method: 'GET',
          url: '/api/texts/refresh'
        }
      });
  });
