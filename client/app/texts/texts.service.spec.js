'use strict';

describe('Service: texts', function () {

  // load the service's module
  beforeEach(module('velociusApp'));

  // instantiate service
  var texts;
  beforeEach(inject(function (_texts_) {
    texts = _texts_;
  }));

  it('should do something', function () {
    expect(!!texts).toBe(true);
  });

});
