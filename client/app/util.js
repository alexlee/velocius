'use strict';

$.fn.reverse = [].reverse;

var COMBINING = XRegExp('\\p{M}', 'g');

function stripCombining(str) {
  return unorm.nfkd(str).replace(COMBINING, '');
}

function clearFormErrors(form) {
  for (var key in form) {
    if (form[key] && form[key].$error) {
      form[key].$error.mongoose = null;
    }
  }
  form.hasErrors = false;
}

function setFormErrors(form, errors) {
  for (var key in errors) {
    form[key].$error.mongoose = errors[key].message;
  }
  form.hasErrors = true;
}

// WARNING: these should match what's on the server
// See: server/config/environment/index.js
var userRoles = ['guest', 'user', 'student', 'teacher', 'admin'];

function hasAccess(role, requiredRole) {
  return (userRoles.indexOf(role) >= userRoles.indexOf(requiredRole));
}

// Creates a callback to set the `accessible` flag on texts,
// by checking against the given role.
function textAccessibilityFn(role) {
  return function(texts) {
    texts.forEach(function(text) {
      text.restricted = text.requiredRole && text.requiredRole !== 'guest';
      text.accessible = hasAccess(role, text.requiredRole);
    });
  };
}
