'use strict';

angular.module('velociusApp')
  .factory('User', function ($resource) {
    return $resource('/api/users/:id/:controller', {
      id: '@_id'
    },
    {
      get: {
        method: 'GET',
        params: {
          id:'me'
        }
      },
      update: {
        method: 'PUT'
      },
      refresh: {
        method: 'GET',
        url: '/api/users/refresh'
      }
	  });
  });
