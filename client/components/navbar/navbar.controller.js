'use strict';

angular.module('velociusApp')
  .controller('NavbarCtrl', function ($scope, $location, $route, Auth) {
    $scope.menu = [
      {
        'title': 'Home',
        'link': '/',
        'icon': 'fa fa-home'
      },
      {
        'title': 'Texts',
        'link': '/texts',
        'icon': 'fa fa-book'
      },
      {
        'title': 'Authors',
        'link': '/authors',
        'icon': 'fa fa-users'
      },
      {
        'title': 'Glosses',
        'link': '/glosses',
        'icon': 'fa fa-comments'
      },
    ];

    Auth.isLoggedInAsync(function(loggedIn) {
      $scope.isLoggedIn = Auth.isLoggedIn();
      $scope.isAdmin = Auth.isAdmin();
      $scope.user = Auth.getCurrentUser();
    });

    $scope.logout = function() {
      Auth.logout();
      $location.path('/');
      $route.reload(); // Need to reload login-related variables
    };

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });
