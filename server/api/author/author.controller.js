'use strict';

var _ = require('lodash');
var Author = require('./author.model');
var Text = require('../text/text.model');

// Get list of authors
exports.index = function(req, res) {
  Author.find()
  .lean().exec(function (err, authors) {
    if(err) { return handleError(res, err); }
    return res.json(200, authors);
  });
};

// Get a single author
exports.show = function(req, res) {
  Author.findById(req.params.id)
  .lean().exec(function (err, author) {
    if(err) { return handleError(res, err); }
    if(!author) { return res.send(404); }
    return res.json(author);
  });
};

// Get an author's texts
exports.authoredTexts = function(req, res) {
  Text.find({authors: req.params.id})
  .populate('authors')
  .populate('editors')
  .select('-body -plain -words')
  .lean().exec(function (err, texts) {
    if(err) { return handleError(res, err); }
    return res.json(200, texts);
  });
};

// Get an author's edited texts
exports.editedTexts = function(req, res) {
  Text.find({editors: req.params.id})
  .populate('authors')
  .populate('editors')
  .select('-body -plain -words')
  .lean().exec(function (err, texts) {
    if(err) { return handleError(res, err); }
    return res.json(200, texts);
  });
};

// Creates a new author in the DB.
exports.create = function(req, res) {
  Author.create(req.body, function(err, author) {
    if(err) { return res.send(400, err); }
    return res.json(201, author);
  });
};

// Updates an existing author in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Author.findById(req.params.id, function (err, author) {
    if (err) { return handleError(res, err); }
    if(!author) { return res.send(404); }
    var updated = _.merge(author, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, author);
    });
  });
};

// Deletes a author from the DB.
exports.destroy = function(req, res) {
  // See if any texts are linked to this author
  var promise =
    Text.find()
    .or([
      {authors: {$in: [req.params.id]}},
      {editors: {$in: [req.params.id]}}
    ])
    .select('_id')
    .count()
    .exec();

  promise.then(function(num_texts) {
    // Throw an error if there are associated texts
    if (num_texts > 0) {
      return handleError(res, "There are texts associated with this author");
    }

    // Otherwise go ahead and try to remove the author
    Author.findById(req.params.id, function (err, author) {
      if(err) { return handleError(res, err); }
      if(!author) { return res.send(404); }
      author.remove(function(err) {
        if(err) { return handleError(res, err); }
        return res.send(204);
      });
    });
  })
};

function handleError(res, err) {
  return res.send(500, err);
}

