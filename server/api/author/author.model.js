'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');

var AuthorSchema = new Schema({
  name: { type: String, required: true, unique: true }
});

AuthorSchema.plugin(
  uniqueValidator,
  {message: "The author/editor ‘{VALUE}’ already exists"}
);

module.exports = mongoose.model('Author', AuthorSchema);
