'use strict';

var _ = require('lodash');
var Gloss = require('./gloss.model');
var glossUtil = require('./gloss.util');

// Get list of glosses
exports.index = function(req, res) {
  Gloss.find({lang: req.params.lang})
  .sort('entry_s')
  .lean().exec(function (err, glosses) {
    if(err) { return handleError(res, err); }
    return res.json(200, glosses);
  });
};

// Get a single gloss
exports.show = function(req, res) {
  Gloss.findOne({_id: req.params.id, lang: req.params.lang})
  .lean().exec(function (err, gloss) {
    if(err) { return handleError(res, err); }
    if(!gloss) { return res.send(404); }
    return res.json(gloss);
  });
};

// Creates a new gloss in the DB.
exports.create = function(req, res) {
  glossUtil.prepareGloss(req.body);
  console.log(JSON.stringify(req.body));
  Gloss.create(req.body, function(err, gloss) {
    if(err) { return handleError(res, err); }
    return res.json(201, gloss);
  });
};

// Updates an existing gloss in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  glossUtil.prepareGloss(req.body);
  console.log(JSON.stringify(req.body));
  Gloss.findById(req.params.id, function (err, gloss) {
    if (err) { return handleError(res, err); }
    if(!gloss) { return res.send(404); }
    var updated = _.merge(gloss, req.body);
    updated.markModified('words');
    updated.markModified('words_s');
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, gloss);
    });
  });
};

// Sets the level of an existing gloss
exports.setLevel = function(req, res) {
  var section = req.body.section;
  var level = req.body.level;
  if (section === undefined || level === undefined) {
    return handleError(res, new Error("Incomplete request"));
  }
  if (level < 0 || level > 3) {
    return handleError(res, new Error("Invalid level setting"));
  }

  Gloss.findById(req.params.id, function (err, gloss) {
    if (err) { return handleError(res, err); }
    if(!gloss) { return res.send(404); }
    gloss.setLevel(section, level);
    gloss.markModified('levels');
    gloss.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, gloss);
    });
  });
};

// Refreshes all glosses in the DB (do this after model changes)
exports.refresh = function(req, res) {
  Gloss.find()
  .exec()
  .then(function success(glosses) {
    glosses.forEach(function (gloss, index, array) {
      glossUtil.prepareGloss(gloss);
      gloss.save(function (err) {
        if (err) { console.log(err); }
      });
    });
    return res.send(200);
  }, function error(err) {
    return handleError(res, err);
  });
};

// Deletes a gloss from the DB.
exports.destroy = function(req, res) {
  Gloss.findById(req.params.id, function (err, gloss) {
    if(err) { return handleError(res, err); }
    if(!gloss) { return res.send(404); }
    gloss.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
