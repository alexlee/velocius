'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');

var langs = require('../lang/langs');

var LevelSchema = new Schema({
  section: { type: Schema.Types.ObjectId, ref: 'Section' },
  level: Number /* 1-3 */
});
var Level = mongoose.model('Level', LevelSchema);

var GlossSchema = new Schema({
  entry: { type: String, required: true }, /* full entry */
  entry_s: { type: String, required: true }, /* for searching */
  words: { type: [String], required: true }, /* divided into words */
  words_s: { type: [String], required: true }, /* for searching */
  w0: { type: String, required: true }, /* first word in entry */
  w1: String, /* second word in entry, if present */
  length: { type: Number, required: true },
  lang: { type: String, enum: langs.shortlangs, required: true },
  lemma: String,
  meaning: String,
  levels: [LevelSchema]
  // pos
});

// Index used when fetching glosses for a given text
GlossSchema.index({ lang: 1, length: 1, w0: 1, w1: 1 });
GlossSchema.index({ lang: 1, entry_s: 1 });

/**
 * Methods
 */
GlossSchema.methods = {
  /**
   * Get the level setting for the given section.
   */
  getLevel: function(section) {
    for (var i = 0; i < this.levels.length; i++) {
      if (this.levels[i].section == section) {
        return this.levels[i];
      }
    }
    return null;
  },

  /**
   * Set the level setting for the given section.
   */
  setLevel: function(section, level) {
    /*
    console.log('Gloss.setLevel');
    console.log('gloss id: '+this._id);
    console.log('section: '+section);
    console.log('level: '+level);
    */

    if (level === 0) {
      this.removeLevel(section);
      return;
    }

    var setting = this.getLevel(section);
    if (setting) {
      //console.log('→ update existing setting');
      setting.level = level;
    } else {
      //console.log('→ add new setting');
      setting = new Level({
        section: section,
        level: level
      });
      this.levels.push(setting);
    }
  },

  /**
   * Remove the level setting for the given section.
   */
  removeLevel: function(section) {
    var i = this.levels.length;
    while (i--) {
      if (this.levels[i].section == section) {
        this.levels.splice(i, 1);
      }
    }
  }
};

module.exports = mongoose.model('Gloss', GlossSchema);
