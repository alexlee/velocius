/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Gloss = require('./gloss.model');

exports.register = function(socket) {
  Gloss.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Gloss.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('gloss:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('gloss:remove', doc);
}