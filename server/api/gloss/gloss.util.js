'use strict';

var XRegExp = require('xregexp').XRegExp;
var unorm = require('unorm');

var COMBINING = XRegExp('\\p{M}', 'g');
var UNICODE_WORD = XRegExp('(\\p{L}+)', 'g');

// Returns the input with all marks and diacritics removed.
var stripCombining = exports.stripCombining = function(str) {
  return unorm.nfkd(str).replace(COMBINING, '');
}

var splitIntoWords = exports.splitIntoWords = function(str) {
  var words = str.match(UNICODE_WORD);
  return (words === null)? [] : words;
};

// Fill in words array and simplified string versions
// TODO process diacritics for Greek
exports.prepareGloss = function(data) {
  var e = data.entry.toLowerCase();
  data.entry_s = stripCombining(e);

  data.words = splitIntoWords(e);
  data.words_s = splitIntoWords(data.entry_s);
  data.length = data.words.length;

  data.w0 = data.words[0];
  data.w1 = (data.length > 1) ? data.words[1] : null;
};
