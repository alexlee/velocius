'use strict';

var express = require('express');
var controller = require('./gloss.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/refresh', auth.hasRole('admin'), controller.refresh);

router.get('/:lang', controller.index);
router.get('/:lang/:id', controller.show);
router.post('/:lang', auth.hasRole('admin'), controller.create);
router.put('/:lang/:id', auth.hasRole('admin'), controller.update);
router.patch('/:lang/:id', auth.hasRole('admin'), controller.update);
router.put('/:lang/:id/level', auth.hasRole('admin'), controller.setLevel);
router.delete('/:lang/:id', auth.hasRole('admin'), controller.destroy);

module.exports = router;
