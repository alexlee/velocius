'use strict';

var _ = require('lodash');

var langs = require('./langs');

// Get list of langs
exports.index = function(req, res) {
  return res.json(200, langs.langs);
};
