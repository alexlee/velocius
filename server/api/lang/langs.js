'use strict';

var _ = require('lodash');

/**
 * Possible languages for texts.
 */
var shortlangs = 'la gr de it fr es en'.split(' ');
var longlangs = 'Latin Greek German Italian French Spanish English'.split(' ');
var langs = []

for (var i in shortlangs) {
  langs.push({
    'short': shortlangs[i],
    'long': longlangs[i]
  });
}

exports.shortlangs = shortlangs;
exports.longlangs = longlangs;
exports.langs = langs;
