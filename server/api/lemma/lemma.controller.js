'use strict';

var _ = require('lodash');
var Lemma = require('./lemma.model');

// Get list of lemmas
exports.index = function(req, res) {
  Lemma.find(function (err, lemmas) {
    if(err) { return handleError(res, err); }
    return res.json(200, lemmas);
  });
};

// Get a single lemma
exports.show = function(req, res) {
  Lemma.findById(req.params.id, function (err, lemma) {
    if(err) { return handleError(res, err); }
    if(!lemma) { return res.send(404); }
    return res.json(lemma);
  });
};

// Creates a new lemma in the DB.
exports.create = function(req, res) {
  Lemma.create(req.body, function(err, lemma) {
    if(err) { return handleError(res, err); }
    return res.json(201, lemma);
  });
};

// Updates an existing lemma in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Lemma.findById(req.params.id, function (err, lemma) {
    if (err) { return handleError(res, err); }
    if(!lemma) { return res.send(404); }
    var updated = _.merge(lemma, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, lemma);
    });
  });
};

// Deletes a lemma from the DB.
exports.destroy = function(req, res) {
  Lemma.findById(req.params.id, function (err, lemma) {
    if(err) { return handleError(res, err); }
    if(!lemma) { return res.send(404); }
    lemma.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}