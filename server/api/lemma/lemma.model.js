'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LemmaSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});

module.exports = mongoose.model('Lemma', LemmaSchema);