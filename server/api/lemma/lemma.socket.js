/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Lemma = require('./lemma.model');

exports.register = function(socket) {
  Lemma.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Lemma.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('lemma:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('lemma:remove', doc);
}