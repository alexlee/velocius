'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');

var SectionSchema = new Schema({
  name: { type: String, required: true, unique: true }
});

SectionSchema.plugin(
  uniqueValidator,
  {message: "The section ‘{VALUE}’ already exists"}
);

module.exports = mongoose.model('Section', SectionSchema);
