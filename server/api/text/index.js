'use strict';

var express = require('express');
var controller = require('./text.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/refresh', auth.hasRole('admin'), controller.refresh);

router.get('/', controller.index);
router.get('/:id', auth.maybeAuthenticated(), controller.show);
router.get('/:id/glosses', auth.maybeAuthenticated(), controller.getGlosses);
router.get('/:id/full', auth.maybeAuthenticated(), controller.getTextAndGlosses);
router.post('/', auth.hasRole('admin'), controller.create);
router.put('/:id', auth.hasRole('admin'), controller.update);
router.patch('/:id', auth.hasRole('admin'), controller.update);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);

module.exports = router;
