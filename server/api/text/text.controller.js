'use strict';

var _ = require('lodash');
var Text = require('./text.model');
var Gloss = require('../gloss/gloss.model');
var textUtil = require('./text.util');
var glossUtil = require('../gloss/gloss.util');
var userRoles = require('../../config/environment').userRoles;

function meetsRequirements(role, requiredRole) {
  return (userRoles.indexOf(role) >= userRoles.indexOf(requiredRole));
}

/**
 * Determines if the given user can access the given text.
 * Returns an HTTP error code on failure, null on success.
 */
function checkAccess(user, text) {
  if (!text) { return 404; }
  var role = user ? user.role : 'guest';
  if (meetsRequirements(role, text.requiredRole)) {
    return null; // ok!
  } else {
    if (!user) return 401; // need authorization
    else return 403; // access forbidden
  }
}

/**
 * Determines the current role for the given request.
 */
function getRole(req) {
  if (req.user) return req.user.role;
  else return 'guest';
}

// Get list of texts
exports.index = function(req, res) {
  //var role = getRole(req);
  Text.find()
  .populate('authors')
  .populate('editors')
  .select('-body -plain -words')
  .lean()
  .exec(function (err, texts) {
    if(err) { return handleError(res, err); }
    return res.json(200, texts);
    //var returnTexts = texts.filter(function(text, index) {
    //  return meetsRequirements(role, text.requiredRole);
    //});
    //return res.json(200, returnTexts);
  });
};

// Get a single text
exports.show = function(req, res) {
  Text.findById(req.params.id)
  .populate('authors')
  .populate('editors')
  .select('-plain -words')
  .lean().exec()
  .then(function success(text) {
    var code = checkAccess(req.user, text);
    if (code) { return res.send(code); }
    return res.json(text);
  }, function error(err) {
    return handleError(res, err);
  });
};

// This query is not as concise as it could be, but of the different
// query/index combinations that I've tried, it makes the most efficient
// use of the index.
// Also, it will still return extra phrase glosses, because it only
// checks whether the first two words in a phrase appear in the text.
// This probably won't scale well for longer texts (where the vocab is
// larger).
function makeGlossQuery(lang, vocab) {
  return {
    $or: [
      // Single word gloss
      {
        lang: lang,
        length: 1,
        'w0': { $in: vocab }
      },
      // Phrase gloss
      {
        lang: lang,
        length: { $gt: 1 },
        'w0': { $in: vocab },
        'w1': { $in: vocab }
      }
    ]
  };
}

// Get the glosses for a text
exports.getGlosses = function(req, res) {
  Text.findById(req.params.id)
  .select('lang words requiresRole')
  .lean().exec()
  .then(function success(text) {
    var code = checkAccess(req.user, text);
    if (code) { return res.send(code); }
    return Gloss.find(makeGlossQuery(text.lang, text.words))
      .lean().exec();
  })
  .then(function success(glosses) {
    return res.json(glosses);
  }, function error(err) {
    console.log(err.message);
    return handleError(res, err);
  });
};

// Get both text and its glosses
exports.getTextAndGlosses = function(req, res) {
  var textRef;
  Text.findById(req.params.id)
  .populate('authors')
  .populate('editors')
  .select('-plain')
  .lean().exec()
  .then(function success(text) {
    var code = checkAccess(req.user, text);
    if (code) { return res.send(code); }
    textRef = text;
    return Gloss.find(makeGlossQuery(text.lang, text.words))
      .lean().exec();
  })
  .then(function success(glosses) {
    delete textRef.words; // client doesn't need this
    return res.json({
      text: textRef,
      glosses: glosses
    });
  }, function error(err) {
    console.log(err.message);
    return handleError(res, err);
  });
};

// Creates a new text in the DB.
exports.create = function(req, res) {
  textUtil.setCachedFields(req.body)
  .then(function success(textData) {
    return Text.create(textData);
  })
  .then(function success(text) {
    return res.json(201, text);
  }, function error(err) {
    return handleError(res, err);
  });
};

// Updates an existing text in the DB.
// FIXME It's only necessary to run setCachedFields if the body has changed,
// but we don't currently check for that. Text updates are infrequent, so
// a fix for this ineffiency is put off for now.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  textUtil.setCachedFields(req.body)
  .then(function success(textData) {
    return Text.findById(req.params.id)
  })
  .then(function success(text) {
    if(!text) { return res.send(404); }
    var updated = _.merge(text, req.body);
    updated.markModified('words');
    console.log(updated);
    return updated.save();
  })
  .then(function success(text) {
    return res.json(200, text);
  },
  function error(err) {
    return handleError(res, err);
  });
};

// Refreshes all texts in the DB (do this after model changes)
exports.refresh = function(req, res) {
  Text.find()
  .exec()
  .then(function success(texts) {
    texts.forEach(function (text, index, array) {
      textUtil.setCachedFields(text)
      .then(function success(text) {
        text.save(function (err) {
          if (err) { console.log(err); }
        });
      });
    });
    return res.send(200);
  }, function error(err) {
    return handleError(res, err);
  });
};

// Deletes a text from the DB.
exports.destroy = function(req, res) {
  Text.findById(req.params.id, function (err, text) {
    if(err) { return handleError(res, err); }
    if(!text) { return res.send(404); }
    text.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
