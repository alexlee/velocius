'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');

var langs = require('../lang/langs');

var TextSchema = new Schema({
  title: { type: String, required: true },
  authors: [{ type: Schema.Types.ObjectId, ref: 'Author' }],
  editors: [{ type: Schema.Types.ObjectId, ref: 'Author' }],
  lang: { type: String, enum: langs.shortlangs, required: true },
  requiredRole: { type: String, required: true, default: 'guest' },
  // created: { type: Date, default: Date.now },
  // modified: { type: Date, default: Date.now },
  // level: ...
  // related: ...
  // notes: ...
  body: String, // original markdown text
  plain: String, // generated plain text
  words: [String] // list of distinct words in text
});

module.exports = mongoose.model('Text', TextSchema);
