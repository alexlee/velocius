'use strict';

var _ = require('lodash');
var XRegExp = require('xregexp').XRegExp;

var showdown = require('showdown');
var converter = new showdown.Converter();
var env = require('jsdom').env;
var mongoose = require('mongoose');

var glossUtil = require('../gloss/gloss.util');

/**
 * Generate the plain text representation of the given Markdown text.
 * Returns a promise.
 */
var markdownToText = exports.markdownToText = function(markdown) {
  markdown = markdown || '';
  var p = new mongoose.Promise;
  var html = '<div id="root">' + converter.makeHtml(markdown) + '</div>';
  env(html, function (errors, window) {
    var $ = require('jquery')(window);
    var plain = $('#root').text();
    p.fulfill(plain);
  });
  return p;
}

var CLAUSE_SEPARATOR = XRegExp('[!?.:;]', 'g');

var splitIntoClauses = exports.splitIntoClauses = function(str) {
  return XRegExp.split(str, CLAUSE_SEPARATOR);
};

var sortAndUniq = exports.sortAndUniq = function(arr) {
  return _.uniq(arr.sort(), true);
};

/**
 * Generate and cache the 'plain' and 'words' fields
 * for the given text data.
 */
exports.setCachedFields = function(textData) {
  return markdownToText(textData.body)
  .then(function success(plain) {
    var plain = plain.toLowerCase();
    var words = glossUtil.splitIntoWords(plain);
    words = sortAndUniq(words);
    textData.plain = plain;
    textData.words = words;
    return textData;
  });
};
