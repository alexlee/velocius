'use strict';

var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var Section = require('../section/section.model');

var validationError = function(res, err) {
  return res.json(422, err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  User.find({})
  .select('-salt -hashedPassword')
  .populate('section')
  .exec(function (err, users) {
    if(err) return res.send(500, err);
    res.json(200, users);
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'user';
  newUser.save(function(err, user) {
    if (err) return validationError(res, err);
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
    res.json({ token: token });
  });
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;
  User.findById(userId)
  .populate('section')
  .exec(function (err, user) {
    if (err) return next(err);
    if (!user) return res.send(401);
    res.json(user.profile);
  });
};

// Refreshes all users in the DB (do this after model changes)
exports.refresh = function(req, res) {
  var defaultSection;
  Section.findOne({name: 'default'})
  .lean().exec()
  .then(function success(section) {
    if (section == null) {
      console.log("Refresh users: could not find default section");
      return res.send(200);
    }
    defaultSection = section;
    return User.find({section: null}).exec();
  })
  .then(function success(users) {
    users.forEach(function (user, index, array) {
      console.log("Setting default section for user: " + user.name);
      user.section = defaultSection._id;
      user.save(function (err) {
        if (err) { console.log(err); }
      });
    });
    return res.send(200);
  }, function error(err) {
    return handleError(res, err);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if(err) return res.send(500, err);
    return res.send(204);
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.send(200);
      });
    } else {
      res.send(403);
    }
  });
};

/**
 * Change a user's role
 */
exports.changeRole = function(req, res) {
  User.findById(req.params.id, function (err, user) {
    if (err) return handleError(res, err);
    if (!user) return res.send(404);
    user.role = req.body.role;
    user.save(function(err) {
      if (err) return handleError(res, err);
      return res.json(200);
    });
  });
};

/**
 * Change a user's section
 */
exports.changeSection = function(req, res) {
  User.findById(req.params.id, function (err, user) {
    if (err) return handleError(res, err);
    if (!user) return res.send(404);
    user.section = req.body.section;
    user.save(function(err) {
      if (err) return handleError(res, err);
      return res.json(200);
    });
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({ _id: userId })
  .select('-salt -hashedPassword') // don't ever give out the password or salt
  .populate('section')
  .exec(function(err, user) {
    if (err) return next(err);
    if (!user) return res.json(401);
    res.json(user);
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};
