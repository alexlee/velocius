var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var Section = require('../../api/section/section.model');

exports.setup = function (User, config) {
  passport.use(new GoogleStrategy({
      clientID: config.google.clientID,
      clientSecret: config.google.clientSecret,
      callbackURL: config.google.callbackURL
    },
    function(accessToken, refreshToken, profile, done) {
      Section.findOne({name: 'default'}).exec(function (err, section) {
        User.findOne({'google.id': profile.id}).exec(function(err, user) {
          if (!user) {
            user = new User({
              name: profile.displayName,
              email: profile.emails[0].value,
              role: 'user',
              section: section._id,
              username: profile.username,
              provider: 'google',
              google: profile._json
            });
            config.resetRole(user);
            user.save(function(err) {
              if (err) return done(err);
              else return done(err, user);
            });
          } else {
            return done(err, user);
          }
        });
      });
    }
  ));
};
