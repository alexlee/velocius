'use strict';

var path = require('path');
var _ = require('lodash');

function requiredProcessEnv(name) {
  if(!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}

/**
 * Is this address in the given domain?
 */
function emailDomainMatch(email, domain) {
  var index = email.indexOf(domain);
  if (index === -1) return false;
  else if (index === email.length - domain.length) return true;
  else return false;
}

/**
 * Examine user and adjust role.
 * This should be changed based on the setup.
 */
function resetRole(user) {
  if (user.email === 'alee@stritahs.com') {
    user.role = 'admin';
    console.log('Set role for ' + user.email + ' to ' + user.role);
  } else if (emailDomainMatch(user.email, 'stritahs.com')) {
    user.role = 'student';
    console.log('Set role for ' + user.email + ' to ' + user.role);
  }
}

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../..'),

  // Server port
  port: process.env.PORT || 9000,

  // Should we populate the DB with sample data?
  seedDB: false,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'velocius-secret'
  },

  // List of user roles
  userRoles: ['guest', 'user', 'student', 'teacher', 'admin'],
  resetRole: resetRole,

  // MongoDB connection options
  mongo: {
    options: {
      db: {
        safe: true
      }
    }
  },

  google: {
    clientID:     process.env.GOOGLE_ID || 'id',
    clientSecret: process.env.GOOGLE_SECRET || 'secret',
    callbackURL:  (process.env.DOMAIN || '') + '/auth/google/callback'
  }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});
