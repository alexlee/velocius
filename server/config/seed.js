/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var mongoose = require('mongoose');

var Author = require('../api/author/author.model');
var Section = require('../api/section/section.model');
var Text = require('../api/text/text.model');
var Gloss = require('../api/gloss/gloss.model');

/* Custom population. */

function setObjectId(obj) {
  return mongoose.Types.ObjectId(obj.$oid);
}

function prepareDocument(doc) {
  doc._id = mongoose.Types.ObjectId(doc._id.$oid);
  delete doc.__v;

  // special handling for Text
  if (doc.authors) {
    doc.authors = doc.authors.map(setObjectId);
  }
  if (doc.editors) {
    doc.editors = doc.editors.map(setObjectId);
  }

  // special handling for Gloss
  if (doc.levels) {
    for (var i = 0; i < doc.levels.length; i++) {
      var setting = doc.levels[i];
      setting._id = setObjectId(setting._id);
      setting.section = setObjectId(setting.section);
    }
  }
  return doc;
}

var models = {
  'authors': Author,
  'sections': Section,
  'texts': Text,
  'glosses': Gloss
};

function addCollection(name) {
  var data = require('./seeds/' + name);
  var model = models[name];
  data = data.map(prepareDocument);
  return model.find({}).remove(function() {
    return model.create(data, function(err, results) {
      if (err) {
        console.log('ERROR while populating ' + name);
        console.log(JSON.stringify(err));
      }
      console.log('finished populating ' + name);
    });
  });
}

addCollection('authors')
  .then(function() {
    return addCollection('sections')
  })
  .then(function() {
    return addCollection('texts')
  })
  .then(function() {
    return addCollection('glosses')
  });

