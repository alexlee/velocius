#!/bin/sh

FLAGS="--db velocius-dev --jsonArray --pretty"
COLLECTIONS="authors texts glosses sections"

for c in $COLLECTIONS
do
	mongoexport $FLAGS --collection $c --out $c.json
done

