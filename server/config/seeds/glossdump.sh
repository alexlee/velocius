#!/bin/sh

FLAGS="--db velocius-dev --jsonArray --pretty"
COLLECTIONS="glosses"

for c in $COLLECTIONS
do
	mongoexport $FLAGS --collection $c --out $c.json
done

